const todoItem = {
    props: ['item', 'index'],
    template: `
    <li class="todo-item todo-item-control-wrapper list-group-item" :class="{completed: item.isCompleted}">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="todo-item-checkbox custom-control-input"
                :id="'check_' + index" v-model="item.isCompleted"
                @change="$emit('complete-change')"
                :disabled="isEditable"
            >
            <label class="custom-control-label" :for="'check_' + index"></label>
        </div>
        <span v-if="!isEditable" class="todo-item-title" @click="setEdit" @focus="setEdit" :tabindex="(isEditable || item.isCompleted) ? '-1' : '0'">
            {{item.title}}
            <i class="fa fa-pencil" v-if="!item.isCompleted"></i>
        </span>
        <input v-if="isEditable" class="todo-item-title-input form-control form-control-sm" 
            v-model="item.title" 
            @keyup.enter="unsetEdit"
            @keyup.esc="unsetEdit"
            @blur="unsetEdit"
            type="text"
            ref="titleinput"
        />
    <button class="todo-item-delete btn btn-light btn-sm" type="button"
        @click="$emit('delete', index)">Delete</button>
    </li>
    `,
    data() {
        return {
            isEditable: false
        }
    },
    methods: {
        setEdit() {
            if (!this.item.isCompleted) {
                this.isEditable = true;
                this.$nextTick(() => {
                    this.$refs.titleinput.focus();
                });
            }
        },

        unsetEdit() {
            this.isEditable = false;
        }
    }
};


